---
title: DRAFT - Image Preparation for Machine Learning
date: "2019-08-04"
description: How do we prepare an image so that it's suitable for consumption by a neural network / machine learning algorithm?
---

## Defining the Problem

Machine learning algorithms require carefully formatted inputs in order to produce satisfactory results. Garbage in, garbage out as they say. 
The data must be formatted properly and how to properly format it is unique to the kind of data. 
Most images are [compressed](https://en.wikipedia.org/wiki/Image_compression) and need to be decoded before feeding them to ML algorithms. 
Additionally, the image may need to be pre-processed to remove noise, artifacts, or otherwise unwanted information.

In this post I'll be exploring how to automate the preparation of one or more images into a format suitable for ML. We'll start off with a JPG image and a CSV of labels and end up with our ML dataset in CSV form.

The image dataset I'll be using consists of 70 JPGs like the one below:
![](./hair.jpg)


As you can see, there is red text near the top which we'll want to remove in our pre-processing step.

## Toolbelt

Much of the ML world is using python and we won't break that trend today. 
After looking around at some available image processing libraries, I've decided to move forward with [scikit-image](https://scikit-image.org/) to do the heavy lifting.
We'll also drop down into [numpy](https://numpy.org/) for a few utility functions to make our lives easier.

## Project Setup
First we'll setup a virtualenv to encapsulate and track our dependencies. Let's create a new virtualenv and activate it:
```bash
virtualenv . && chmod +x bin/activate && source bin/activate
``` 

And now let's install our dependencies, making sure to save them to our requirements file:
```bash
pip install scikit-image && pip freeze > requirements.txt
``` 

<p>Take a second to confirm that we've installed everything correctly:</p>

```bash 
python <<EOF
from skimage import data, io, filters

image = data.coins()
edges = filters.sobel(image)
io.imshow(edges)
io.show()
EOF
``` 

You should see the following image appear if everything is working correctly:

![](./coins-sobel.png)

Great! Now we're ready to start working on our script to process the images.


# To be continued...